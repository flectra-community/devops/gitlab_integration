{
    'name': "gitlab_integration",

    'summary': """
        Monitor Gitlab Repositories, Pipelines and Jobs containing Flectra Modules""",
    'author': "Jamotion GmbH",
    'website': "https://jamotion.ch",
    'category': 'Specific Industry Applications',
    'version': '1.0.1.0.0',
    'depends': [
        'flectra_community',
        'website',
    ],
    'data': [
        'data/data.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}