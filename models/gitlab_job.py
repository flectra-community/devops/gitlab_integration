import json
import logging
from urllib import request

from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabJob(models.Model):
    # Private attributes
    _name = 'gitlab.job'
    _description = 'Gitlab Job'
    _order = 'created_at DESC'

    # Default methods

    # Fields declaration
    name = fields.Char(
            string="Name",
            required=True,
    )

    job_id = fields.Integer(
            string="Job ID",
            required=True,
    )

    pipeline_id = fields.Integer(
            string="Pipeline ID",
            required=True,
            index=True,
    )

    repository_id = fields.Many2one(
            comodel_name="gitlab.repository",
            string="Repository",
            required=True,
            ondelete='cascade',
    )

    branch_id = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Branch",
            required=True,
            ondelete='cascade',
    )

    stage = fields.Char(
            string="Stage",
    )

    status = fields.Char(
            string="Status",
    )

    web_url = fields.Char(
            string="Url",
    )

    duration = fields.Float(
            string="Duration",
    )

    created_at = fields.Datetime(string="Created at")
    started_at = fields.Datetime(string="Started at")
    finished_at = fields.Datetime(string="Finished at")

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges
    _sql_constraints = [
        ('unique_job_id', 'unique (job_id)', 'A job could only exist once !')
    ]
    # CRUD methods

    # Action methods

    # Business methods
    @api.model
    def _update_running_jobs(self):
        repos = self.search([('status', '=', 'running')]).mapped('repository_id')
        if repos:
            self._sync_jobs(repos)

    @api.model
    def _sync_jobs(self, repositories=False):
        token = self.env['ir.config_parameter'].sudo().get_param('gitlab.private_token')
        if not repositories:
            repositories = self.env['gitlab.repository'].search([])

        for repo in repositories:
            _logger.debug('Updating jobs of repository %s...', repo.name)
            for pipeline in repo.pipeline_ids.filtered(
                    lambda f: not f.job_ids or f.status in ['pending', 'running', 'manual', 'scheduled']):
                req = request.Request(
                        url='https://gitlab.com/api/v4/projects/%s/pipelines/%s/jobs' % (
                            repo.repo_id, pipeline.pipeline_id),
                        method='GET',
                        headers={
                            'Private-Token': token,
                        }
                )
                response = request.urlopen(req)
                jobs = json.loads(response.read().decode('utf-8'))
                job_ids = [j['id'] for j in jobs]

                existing_jobs = self.search([('job_id', 'in', job_ids)])

                for job in jobs:
                    if job['status'] == 'skipped':
                        continue

                    branch = repo.branch_ids.filtered(lambda f: f.name == job['ref'])
                    if branch:
                        existing = existing_jobs.filtered(lambda f: f.job_id == job['id'])
                        if existing:
                            _logger.debug('Job %s of repository %s found, updating data...', existing.name,
                                          repo.name)
                            existing.write({
                                'status': job['status'],
                                'duration': job['duration'],
                                'created_at': job['created_at'],
                                'started_at': job['started_at'],
                                'finished_at': job['finished_at'],
                            })
                        else:
                            _logger.debug('Job %s of repository %s does not exist, creating job...',
                                          branch['name'], repo.name)
                            self.create({
                                'job_id': job['id'],
                                'repository_id': repo.id,
                                'branch_id': branch.id,
                                'pipeline_id': pipeline.id,
                                'name': job['name'],
                                'stage': job['stage'],
                                'status': job['status'],
                                'web_url': job['web_url'],
                                'duration': job['duration'],
                                'created_at': job['created_at'],
                                'started_at': job['started_at'],
                                'finished_at': job['finished_at'],
                            })
