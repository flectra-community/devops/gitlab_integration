import json
import logging
from urllib import request

from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabRepository(models.Model):
    # Private attributes
    _name = 'gitlab.repository'
    _description = 'Gitlab Repository'
    _order = 'path'

    # Default methods

    # Fields declaration
    name = fields.Char(
            string='Name',
            required=True,
    )

    description = fields.Text(
            string="Description",
    )

    repo_id = fields.Integer(
            string="Gitlab ID",
    )

    path = fields.Char(
            string='Path',
            required=True,
    )

    visibility = fields.Char(
            string='Visibility',
    )

    open_issues = fields.Integer(
            string="Open Issues",
    )

    group = fields.Char(
            string='Group',
            required=True,
            default='flectra-community',
    )

    web_url = fields.Char(
            string='Web Link',
    )

    merge_requests_link = fields.Char(
            string='Merge Requests Link',
    )

    issues_link = fields.Char(
            string='Issues Link',
    )
    members_link = fields.Char(
            string='Members Link',
    )

    events_link = fields.Char(
            string='Events Link',
    )
    labels_link = fields.Char(
            string='Labels Link',
    )
    branches_link = fields.Char(
            string='Branches link',
    )

    branch_ids = fields.One2many(
            comodel_name="gitlab.branch",
            inverse_name="repository_id",
            string="Branches",
    )

    default_branch = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Default Branch",
            compute='_compute_default_branch',
    )

    upstream_branch = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Default Branch",
            compute='_compute_default_branch',
    )

    fixed_branch = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Fixed Branch",
            compute='_compute_default_branch',
    )

    production_branch = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Default Branch",
            compute='_compute_default_branch',
    )

    is_uptodate = fields.Boolean(
            string="Up-to-date",
            compute='_compute_default_branch',
    )

    pipeline_ids = fields.One2many(
            comodel_name="gitlab.pipeline",
            inverse_name="repository_id",
            string="Pipelines",
    )

    latest_pipeline = fields.Many2one(
            comodel_name="gitlab.pipeline",
            string="Latest Pipeline",
            compute='_compute_latest_pipeline',
    )

    job_ids = fields.One2many(
            comodel_name="gitlab.job",
            inverse_name="repository_id",
            string="Jobs",
    )

    module_ids = fields.One2many(
            comodel_name="gitlab.module",
            inverse_name="repository_id",
            string="Modules",
    )

    depends_on = fields.Many2many(
            comodel_name="gitlab.repository",
            relation="gitlab_repository_dependency",
            column1="depends_on",
            column2="used_by",
            string="Depends on",
    )

    depending_status = fields.Char(
            string="Depending Status",
            compute='_compute_depending_status',
    )

    used_by = fields.Many2many(
            comodel_name="gitlab.repository",
            relation="gitlab_repository_dependency",
            column1="used_by",
            column2="depends_on",
            string="Used by",
    )

    state = fields.Selection(
            string="State",
            selection=[
                ('current', 'Up-to-date'),
                ('update', 'Needs Update'),
                ('review', 'Review'),
                ('new', 'New'),
            ],
            compute='_compute_state',
    )

    # compute and search fields, in the same order that fields declaration
    @api.multi
    def _compute_depending_status(self):
        for record in self:
            status = 'success'
            for dep in record.depends_on:
                if dep.fixed_branch and dep.fixed_branch.latest_pipeline:
                    if status == 'success' and dep.fixed_branch.latest_pipeline.status != 'success':
                        status = dep.fixed_branch.latest_pipeline.status
                    elif status == 'running' and dep.fixed_branch.latest_pipeline.status == 'failed':
                        status = 'failed'
                        break
            record.depending_status = status

    @api.multi
    def _compute_state(self):
        for record in self:
            if not record.production_branch or not record.fixed_branch or not record.upstream_branch:
                record.state = 'new'
            elif record.upstream_branch.last_commit_date > record.production_branch.last_commit_date:
                if record.production_branch.last_commit_date < record.fixed_branch.last_commit_date < record.upstream_branch.last_commit_date:
                    record.state = 'update'
                else:
                    record.state = 'review'
            else:
                record.state = 'current'

    @api.multi
    def _compute_default_branch(self):
        for record in self:
            record.default_branch = record.branch_ids.filtered(lambda f: f.default)
            record.upstream_branch = record.branch_ids.filtered(lambda f: f.name == '1.0-upstream')
            record.production_branch = record.branch_ids.filtered(lambda f: f.name == '1.0')
            record.fixed_branch = record.branch_ids.filtered(lambda f: f.name == '1.0-fixed')
            if record.production_branch and record.upstream_branch:
                record.is_uptodate = record.production_branch.last_commit_date >= record.upstream_branch.last_commit_date
            else:
                record.is_uptodate = True

    @api.multi
    def _compute_latest_pipeline(self):
        for record in self:
            if record.pipeline_ids:
                pipelines = record.pipeline_ids.sorted(key='pipeline_id', reverse=True)
                if pipelines:
                    record.latest_pipeline = pipelines[0]

    # Constraints and onchanges
    _sql_constraints = [
        ('unique_repository_id', 'unique (repo_id)', 'A repository could only exist once !')
    ]

    # CRUD methods

    # Action methods
    @api.multi
    def action_update(self, branches=True, pipelines=True, jobs=True, master=True):
        if branches:
            self.env['gitlab.branch']._sync_branches(self)
        if pipelines:
            self.env['gitlab.pipeline']._sync_pipelines(self)
        if jobs:
            self.env['gitlab.job']._sync_jobs(self)

        if master:
            self.update_master()

    @api.multi
    def update_master(self):
        # Abstract method for overrides only
        self.ensure_one()

    # Business methods
    @api.model
    def _sync_repositories(self):
        existing_repos = self.search([])
        _logger.debug('Calling gitlab.com for group informations...')
        response = request.urlopen('https://gitlab.com/api/v4/groups/flectra-community')
        group = json.loads(response.read().decode('utf-8'))
        repos = group['projects']
        _logger.debug('%s repositories received from gitlab.com', len(repos))
        updated_repo_ids = []

        for repo in repos:
            existing = existing_repos.filtered(lambda f: f.repo_id == repo['id'])
            if existing:
                updated_repo_ids.append(existing.id)
                _logger.debug('Repository %s already exists, updating data.', existing.name)
                try:
                    existing.write({
                        'name': repo['name'],
                        'path': repo['path'],
                        'visibility': repo['visibility'],
                        'open_issues': repo['open_issues_count'],
                        'web_url': repo['web_url'],
                        'group': repo['namespace']['name'],
                        'description': repo['description'],
                        'merge_requests_link': repo['_links']['merge_requests'],
                        'issues_link': repo['_links']['merge_requests'],
                        'members_link': repo['_links']['members'],
                        'events_link': repo['_links']['events'],
                        'labels_link': repo['_links']['labels'],
                        'branches_link': repo['_links']['repo_branches'],
                    })
                    self.env['gitlab.branch']._sync_branches(existing)
                    self.env['gitlab.pipeline']._sync_pipelines(existing)
                    self.env['gitlab.job']._sync_jobs(existing)
                except Exception as ex:
                    _logger.error('Update failed with error: %s', str(ex))
            else:
                _logger.debug('New repository %s found, creating data.', repo['name'])
                try:
                    new_repo = self.create({
                        'name': repo['name'],
                        'path': repo['path'],
                        'visibility': repo['visibility'],
                        'open_issues': repo['open_issues_count'],
                        'web_url': repo['web_url'],
                        'repo_id': repo['id'],
                        'group': repo['namespace']['name'],
                        'description': repo['description'],
                        'merge_requests_link': repo['_links']['merge_requests'],
                        'issues_link': repo['_links']['merge_requests'],
                        'members_link': repo['_links']['members'],
                        'events_link': repo['_links']['events'],
                        'labels_link': repo['_links']['labels'],
                        'branches_link': repo['_links']['repo_branches'],
                    })
                    self.env['gitlab.branch']._sync_branches(new_repo)
                    self.env['gitlab.pipeline']._sync_pipelines(new_repo)
                    self.env['gitlab.job']._sync_jobs(new_repo)
                except Exception as ex:
                    _logger.error('Insert failed with error: %s', str(ex))

        existing_repos.filtered(lambda f: f.id not in updated_repo_ids).unlink()
