import json
import logging
from urllib import request

from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabPipeline(models.Model):
    # Private attributes
    _name = 'gitlab.pipeline'
    _description = 'gitlab.pipeline'
    _order = 'pipeline_id DESC'

    # Default methods

    # Fields declaration
    name = fields.Char(
            string="Name",
            compute='_compute_name',
    )

    pipeline_id = fields.Integer(
            string="Pipeline ID",
            required=True,
            index=True,
    )

    repository_id = fields.Many2one(
            comodel_name="gitlab.repository",
            string="Repository",
            required=True,
            ondelete='cascade',
    )

    branch_id = fields.Many2one(
            comodel_name="gitlab.branch",
            string="Branch",
            required=True,
            ondelete='cascade',
    )

    job_ids = fields.One2many(
            comodel_name="gitlab.job",
            inverse_name="pipeline_id",
            string="Jobs",
    )

    status = fields.Char(
            string="Status",
    )

    web_url = fields.Char(
            string="Url",
    )
    jobs_failed = fields.Integer(
            string="# Jobs Failed",
            compute='_compute_jobs',
    )

    jobs_running = fields.Integer(
            string="# Jobs Running",
            compute='_compute_jobs',
    )

    jobs_success = fields.Integer(
            string="# Jobs OK",
            compute='_compute_jobs',
    )

    # compute and search fields, in the same order that fields declaration
    def _compute_jobs(self):
        for record in self:
            record.jobs_failed = len(record.job_ids.filtered(lambda f: f.status == 'failed'))
            record.jobs_success = len(record.job_ids.filtered(lambda f: f.status == 'success'))
            record.jobs_running = len(record.job_ids.filtered(lambda f: f.status == 'running'))

    def _compute_name(self):
        for record in self:
            record.name = '%s - %s' % (record.pipeline_id, record.status)

    # Constraints and onchanges
    _sql_constraints = [
        ('unique_pipeline_id', 'unique (pipeline_id)', 'A pipeline could only exist once !')
    ]

    # CRUD methods

    # Action methods

    # Business methods
    @api.model
    def _update_running_pipelines(self):
        repos = self.search([('status', '=', 'running')]).mapped('repository_id')
        if repos:
            self._sync_pipelines(repos)

    @api.model
    def _sync_pipelines(self, repositories=False):
        token = self.env['ir.config_parameter'].sudo().get_param('gitlab.private_token')
        if not repositories:
            repositories = self.env['gitlab.repository'].search([])

        for repo in repositories:
            _logger.debug('Updating pipelines of repository %s...', repo.name)
            req = request.Request(
                    url='https://gitlab.com/api/v4/projects/%s/pipelines' % repo.repo_id,
                    method='GET',
                    headers={
                        'Private-Token': token,
                    }
            )
            response = request.urlopen(req)
            pipelines = json.loads(response.read().decode('utf-8'))
            pipeline_ids = [p['id'] for p in pipelines]

            existing_pipelines = self.search([('pipeline_id', 'in', pipeline_ids)])

            for pipeline in pipelines:
                branch = repo.branch_ids.filtered(lambda f: f.name == pipeline['ref'])
                if branch:
                    existing = existing_pipelines.filtered(lambda f: f.pipeline_id == pipeline['id'])
                    if existing:
                        _logger.debug('Pipeline %s of repository %s found, updating data...', pipeline['id'], repo.name)
                        existing.write({
                            'status': pipeline['status'],
                        })
                    else:
                        _logger.debug('Pipeline %s of repository %s does not exist, creating branch...',
                                      branch['name'], repo.name)
                        self.create({
                            'repository_id': repo.id,
                            'branch_id': branch.id,
                            'pipeline_id': pipeline['id'],
                            'status': pipeline['status'],
                            'web_url': pipeline['web_url'],
                        })
