import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabModule(models.Model):
    # Private attributes
    _name = 'gitlab.module'
    _description = 'Gitlab Module'

    # Default methods

    # Fields declaration
    name = fields.Char(
            string="Name",
            required=True,
    )

    repository_id = fields.Many2one(
            comodel_name="gitlab.repository",
            string="Repository",
            ondelete="cascade",
    )

    depends_on = fields.Many2many(
            comodel_name="gitlab.module",
            relation="gitlab_module_dependency",
            column1="depends_on",
            column2="used_by",
            string="Depends on",
    )

    used_by = fields.Many2many(
            comodel_name="gitlab.module",
            relation="gitlab_module_dependency",
            column1="used_by",
            column2="depends_on",
            string="Used by",
    )

    # compute and search fields, in the same order that fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
