import json
import logging
from urllib import request

from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class GitlabBranch(models.Model):
    # Private attributes
    _name = 'gitlab.branch'
    _description = 'Gitlab Branch'
    _order = 'name'

    # Default methods

    # Fields declaration
    name = fields.Char(
            string="Name",
            required=True,
    )

    repository_id = fields.Many2one(
            comodel_name="gitlab.repository",
            string="Repository",
            required=True,
            ondelete='cascade',
    )

    protected = fields.Boolean(
            string="Protected",
    )

    default = fields.Boolean(
            string="Default",
    )

    last_commit_date = fields.Datetime(
            string="Last Commit date",
    )

    last_commit_title = fields.Char(
            string="Last Commit Title",
    )

    last_commit_message = fields.Text(
            string="Last Commit Message",
    )

    last_commit_author_name = fields.Char(
            string="Last Commit Author",
            required=False,
    )

    last_commit_author_email = fields.Char(
            string="Last Commit Author eMail",
            required=False,
    )

    pipeline_ids = fields.One2many(
            comodel_name="gitlab.pipeline",
            inverse_name="branch_id",
            string="Pipelines",
    )

    latest_pipeline = fields.Many2one(
            comodel_name="gitlab.pipeline",
            string="Latest Pipeline",
            compute='_compute_latest_pipeline',
    )

    # compute and search fields, in the same order that fields declaration
    def _compute_latest_pipeline(self):
        for record in self:
            record.latest_pipeline = record.pipeline_ids and record.pipeline_ids[0] or False

    # Constraints and onchanges
    _sql_constraints = [
        ('unique_branch_name', 'unique (repository_id, name)', 'A branch could only exist once for repository!')
    ]
    # CRUD methods

    # Action methods

    # Business methods
    @api.model
    def _sync_branches(self, repositories=False):

        if not repositories:
            repositories = self.env['gitlab.repository'].search([])

        for repo in repositories:
            _logger.debug('Updating branches of repository %s...', repo.name)
            response = request.urlopen('https://gitlab.com/api/v4/projects/%s/repository/branches' % repo.repo_id)
            branches = json.loads(response.read().decode('utf-8'))
            found_branch_ids = []
            for branch in branches:
                existing = repo.branch_ids.filtered(lambda f: f.name == branch['name'])
                if existing:
                    _logger.debug('Branch %s of repository %s found, updating data...', existing.name, repo.name)
                    existing.write({
                        'protected': branch['protected'],
                        'default': branch['default'],
                        'last_commit_date': branch['commit']['committed_date'],
                        'last_commit_title': branch['commit']['title'],
                        'last_commit_message': branch['commit']['message'],
                        'last_commit_author_name': branch['commit']['author_name'],
                        'last_commit_author_email': branch['commit']['author_email'],
                    })
                else:
                    _logger.debug('Branch %s of repository %s does not exist, creating branch...',
                                  branch['name'], repo.name)
                    existing = self.create({
                        'repository_id': repo.id,
                        'name': branch['name'],
                        'protected': branch['protected'],
                        'default': branch['default'],
                        'last_commit_date': branch['commit']['committed_date'],
                        'last_commit_title': branch['commit']['title'],
                        'last_commit_message': branch['commit']['message'],
                        'last_commit_author_name': branch['commit']['author_name'],
                        'last_commit_author_email': branch['commit']['author_email'],
                    })
                found_branch_ids.append(existing.id)

            to_del = repo.branch_ids.filtered(lambda f: f.id not in found_branch_ids)
            if to_del:
                _logger.debug('%s no longer used branches of repository %s found, deleting them...',
                              len(to_del), repo.name)
                to_del.unlink()
