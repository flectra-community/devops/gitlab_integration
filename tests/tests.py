from flectra.tests import common
from flectra.exceptions import ValidationError


class Tests(common.TransactionCase):
    def setUp(self):
        super(Tests, self).setUp()

    def test_sample_one(self):
        # Test description
        self.assertEqual('1', '1')

        with self.assertRaises(ValidationError):
            raise ValidationError