$(function () {
    $('#search').on('input', function () {
        filterRepos();
    });

    $('.fc_state_sel, .fc_dep_sel, .fc_ub_sel, .fc_fb_sel, .fc_pb_sel').click(function () {
        $(this).toggleClass("btn-default btn-primary");
        filterRepos();
        $('#search').focus();
    });

    function filterRepos() {
        $('.fc_repo').hide();
        $('.fc_state_sel.btn-primary').each(function () {
            $('.fc_repo.fc_' + $(this).attr('data-state')).show();
        });
        $('.fc_dep_sel.btn-default').each(function () {
            $('.fc_repo.fc_dep_' + $(this).attr('data-state')).hide();
        });
        $('.fc_ub_sel.btn-default').each(function () {
            $('.fc_repo.fc_ub_' + $(this).attr('data-state')).hide();
        });
        $('.fc_fb_sel.btn-default').each(function () {
            $('.fc_repo.fc_fb_' + $(this).attr('data-state')).hide();
        });
        $('.fc_pb_sel.btn-default').each(function () {
            $('.fc_repo.fc_pb_' + $(this).attr('data-state')).hide();
        });
        if ($('#search').val().length >= 1) {
            $('div.panel-heading > span:not(:contains(' + $('#search').val() + '))').parent().parent().parent().hide();
        }
    }
});
