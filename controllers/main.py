import json

import werkzeug

from flectra import http

import logging

_logger = logging.getLogger(__name__)


class GitlabIntegration(http.Controller):

    @http.route('/gitlab/repository', auth='public', website=True)
    def repository_list(self, **kw):
        repos = http.request.env['gitlab.repository'].search([])
        return http.request.render('gitlab_integration.repository_list', {
            'repos': repos,
        })

    @http.route('/gitlab/repository/<model("gitlab.repository"):repository>', auth='public', website=True)
    def repository_detail(self, repository, **kw):
        return http.request.render('gitlab_integration.repository_detail', {
            'repo': repository,
        })

    @http.route('/gitlab/repository/update/<model("gitlab.repository"):repository>', auth='public', website=True)
    def repository_update(self, repository, **kw):
        repository.sudo().action_update()
        return http.request.redirect('/gitlab/repository/%s' % repository.id)

    @http.route('/gitlab/hook', type='json', auth='none', methods=['POST'], csrf=False)
    def repository_hook(self, **post):
        env = http.request.env

        token = http.request.httprequest.headers.get('X-Gitlab-Token')
        if token != 'FlectraCommunityGitLabUpdate':
            return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'error': 'Invalid token',
                        'error_descrip': 'Token not valid',
                    }))

        event = http.request.httprequest.headers.get('X-Gitlab-Event')
        data = http.request.jsonrequest
        repo_id = False
        update_branches = False
        update_pipelines = False
        update_jobs = False
        update_master = False

        if event in ['Push Hook', 'Tag Push Hook']:
            repo_id = data['project']['id']
            update_branches = True

        if event in ['Merge Request Hook']:
            repo_id = data['project']['id']
            update_branches = True
            if data['object_attributes']['target_branch'] == '1.0' and \
                    data['object_attributes']['state'] == 'merged':
                update_master = True

        if event in ['Pipeline Hook']:
            repo_id = data['project']['id']
            update_pipelines = True

        if event in ['Build Hook']:
            repo_id = data['project_id']
            update_jobs = True

        if not repo_id:
            return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'error': 'Event not supported',
                        'error_descrip': 'Event %s not supported' % event,
                    }))
        repository = env['gitlab.repository'].sudo().search([('repo_id', '=', repo_id)])
        if not repository:
            return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'error': 'Project not found',
                        'error_descrip': 'No project for given id found',
                    }))

        try:
            repository.sudo().action_update(branches=update_branches, pipelines=update_pipelines, jobs=update_jobs,
                                            master=update_master)
        except:
            pass
        return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
        )

    @http.route('/gitlab/dependencies/repo/<token>', type='json', auth='none', methods=['POST'], csrf=False)
    def import_repo_dependencies(self, token, **post):
        env = http.request.env
        if token != 'FlectraCommunityRepoUpdate':
            return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'error': 'Invalid token',
                        'error_descrip': 'Token not valid',
                    }))

        all_repos = env['gitlab.repository'].sudo().search([])

        repos = http.request.jsonrequest
        for name, data in repos.items():
            existing = all_repos.filtered(lambda f: f.name == name)
            if not existing:
                continue

            for depends in data['depends_on']:
                depending = all_repos.filtered(lambda f: f.name == depends)
                if not depending or depending in existing.depends_on:
                    continue
                existing.depends_on = [(4, depending.id, False)]

            for used in data['used_by']:
                using = all_repos.filtered(lambda f: f.name == used)
                if not using or using in existing.used_by:
                    continue
                existing.used_by = [(4, using.id, False)]

        return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
        )

    @http.route('/gitlab/dependencies/mods/<token>', type='json', auth='none', methods=['POST'], csrf=False)
    def import_mods_dependencies(self, token, **post):
        env = http.request.env
        if token != 'FlectraCommunityRepoUpdate':
            return werkzeug.wrappers.Response(
                    status=400,
                    content_type='application/json; charset=utf-8',
                    response=json.dumps({
                        'error': 'Invalid token',
                        'error_descrip': 'Token not valid',
                    }))

        all_repos = env['gitlab.repository'].sudo().search([])
        all_mods = env['gitlab.module'].sudo().search([])

        mods = http.request.jsonrequest
        for name, data in mods.items():
            existing = all_mods.filtered(lambda f: f.name == name)
            if not existing and not data['repo']:
                continue

            if not existing:
                curr_repo = all_repos.filtered(lambda f: f.name == data['repo'])
                if not curr_repo:
                    continue

                existing = env['gitlab.module'].sudo().create({
                    'repository_id': curr_repo.id,
                    'name': name,
                })

            for depends in data['depends_on']:
                depending = all_mods.filtered(lambda f: f.name == depends)
                if not depending or depending in existing.depends_on:
                    continue
                existing.depends_on = [(4, depending.id, False)]

            for used in data['used_by']:
                using = all_mods.filtered(lambda f: f.name == used)
                if not using or using in existing.used_by:
                    continue
                existing.used_by = [(4, using.id, False)]

        return werkzeug.wrappers.Response(
                status=200,
                content_type='application/json; charset=utf-8',
        )
